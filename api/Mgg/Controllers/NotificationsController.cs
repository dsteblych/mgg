﻿//using System.Threading.Tasks;
//using System.Web.Http;
//using Mgg.Controllers;
//using Mgg.Notification;
//using Microsoft.Azure.NotificationHubs;

//namespace Mgg.WebApi.Controllers
//{
//    /// <summary>
//    /// Provide azure push notification api.
//    /// </summary>
//    /// <seealso cref="BaseController" />
//    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CC0091:Use static method", Justification = "Api methods calls in runtime")]
//    [Route("api/[controller]")]
//    public class NotificationsController : BaseController
//    {
//        private readonly INotificationsService notificationsService;

//        //public NotificationsController(INotificationsService notificationsService)
//        //{
//        //    notificationsService = notificationsService;
//        //}


//        /// <summary>
//        /// Creates a registration id
//        /// </summary>
//        /// <param name="handle">The notification handle.</param>
//        /// <returns>Registration id.</returns>
//        [HttpPost]
//        [Route("notifications/create")]
//        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
//        public async Task<string> CreateAsync(string handle = null)
//        {
//            return await notificationsService.CreateAsync(handle);
//        }

//        /// <summary>
//        /// Creates or updates a registration (with provided channelURI) at the specified id.
//        /// </summary>
//        /// <param name="id">The notification identifier.</param>
//        /// <param name="deviceUpdate">The device registration parametrs.
//        /// Tags is key/value dictionary that contains data for registration.
//        /// lang push language,
//        /// tenant - tenant code
//        /// </param>
//        /// <returns>The <see cref="Task{HttpResponseMessage}"/>.</returns>
//        /// <exception cref="HttpResponseException">Throw exception if Platform is unknown.</exception>
//        [HttpPost]
//        [Route("notifications/register/{id}")]
//        //[SwaggerResponse(HttpStatusCode.OK)]
//        public async Task<RegistrationDescription> RegisterAsync(string id, DeviceRegistration deviceUpdate)
//        {
//            return await notificationsService.RegisterAsync(id, deviceUpdate);
//        }

//        /// <summary>
//        /// Deletes registration by identifier.
//        /// </summary>
//        /// <param name="id">The notification identifier.</param>
//        /// <returns>The <see cref="Task{HttpResponseMessage}"/>.</returns>
//        [HttpDelete]
//        [Route("notifications/{id}")]
//        // [SwaggerResponse(HttpStatusCode.OK)]
//        public async Task DeleteAsync(string id)
//        {
//            await notificationsService.DeleteAsync(id);
//        }
//    }
//}
