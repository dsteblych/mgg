﻿using System.Collections.Generic;

namespace Mgg.Notification
{
    public class DeviceRegistration
    {
        public string Platform { get; set; }

        public string Handle { get; set; }

        public Dictionary<string, string> Tags { get; set; }
    }
}
