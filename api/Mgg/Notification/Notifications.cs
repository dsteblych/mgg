﻿using Microsoft.Azure.NotificationHubs;

namespace Mgg.Notification
{
    public sealed class Notifications
    {
        private static readonly object SyncRoot = new object();

        private static volatile Notifications instance;

        private Notifications()
        {
            Hub = NotificationHubClient.CreateClientFromConnectionString("<your hub's DefaultFullSharedAccessSignature>", "<hub name>");
        }

        public static Notifications Instance
        {

            get
            {
                if (instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Notifications();
                        }
                    }
                }

                return instance;
            }
        }

        public NotificationHubClient Hub { get; }
    }
}
