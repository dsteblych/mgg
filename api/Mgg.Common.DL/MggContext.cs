﻿using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

using Mgg.Common.DL.Entities;
using Mgg.Common.DL.Entities.Configurations;

using Microsoft.EntityFrameworkCore;

namespace Mgg.Common.DL
{
    internal class MggContext : DbContext
    {
        [NotNull]
        [SuppressMessage("ReSharper", "NotNullMemberIsNotInitialized", Justification = "Property initialized using reflection")]
        public DbSet<Item> Items { get; set; }

        protected override void OnConfiguring([NotNull]DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=mgg.db");
        }

        protected override void OnModelCreating([NotNull]ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ItemMapping());
        }
    }
}