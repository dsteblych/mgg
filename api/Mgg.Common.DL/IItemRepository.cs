﻿using Mgg.Common.DL.Entities;

namespace Mgg.Common.DL
{
    public interface IItemRepository : IRepository<Item, int>
    {
    }
}