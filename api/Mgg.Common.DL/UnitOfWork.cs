﻿using JetBrains.Annotations;

namespace Mgg.Common.DL
{
    using System.Linq;

    using Microsoft.EntityFrameworkCore.Storage;

    public class UnitOfWork : IUnitOfWork
    {
        [NotNull]
        private readonly MggContext context = new MggContext();

        private readonly ISession _session;
        private readonly IDbContextTransaction transaction;
        private bool isAlive = true;
        private bool isCommitted;

        public UnitOfWork([NotNull]IItemRepository itemRepository, [NotNull]IDbContextTransaction transaction)
        {
            ItemRepository = itemRepository;
            this.transaction = transaction;
        }

        public IItemRepository ItemRepository { get; }

        public void Commit()
        {
            if (!isAlive)
            {
                return;
            }

            isCommitted = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (!isAlive)
            {
                return;
            }

            isAlive = false;

            try
            {
                if (isCommitted)
                {
                    transaction.Commit();
                }
            }
            finally
            {
                transaction.Dispose();
                _session.Dispose();
            }
        }

        internal Maybe<T> Get<T>(long id) where T : class
        {
            return _session.Get<T>(id);
        }

        internal void SaveOrUpdate<T>(T entity)
        {
            _session.SaveOrUpdate(entity);
        }

        internal void Delete<T>(T entity)
        {
            _session.Delete(entity);
        }

        internal IQueryable<T> Query<T>()
        {
            return _session.Query<T>();
        }
    }

    internal interface ISession
    {
        void Dispose();

        void Delete(object entity);

        IQueryable<T> Query<T>();

        void SaveOrUpdate(object entity);

        Maybe<T> Get<T>(long id) where T : class;
    }
}
