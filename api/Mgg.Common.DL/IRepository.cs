﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Mgg.Common.DL.Entities;

namespace Mgg.Common.DL
{
    public interface IRepository<TTEntity, TId> where TTEntity : Entity<TId> where TId : IEquatable<TId>
    {
        IQueryable<TTEntity> All { get; }

        IEnumerable<TTEntity> AllAsEnumerable { get; }

        IQueryable<TTEntity> Where(Expression<Func<TTEntity, bool>> predicate);

        Maybe<TTEntity> FindById(TId id);

        TTEntity Load(TId id);

        TId Add(TTEntity newEntity);

        void Remove(TTEntity entity);
    }
}