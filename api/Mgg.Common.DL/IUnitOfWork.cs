﻿using System;

using JetBrains.Annotations;

namespace Mgg.Common.DL
{
    public interface IUnitOfWork : IDisposable
    {
        [NotNull]
        IItemRepository ItemRepository { get; }

        void Save();
    }
}
