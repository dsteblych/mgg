﻿
// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Nullability", "RINUL:Property is missing item nullability annotation.", Justification = "Property initialized using reflection", Scope = "member", Target = "~P:Mgg.Common.DL.MggContext.Items")]