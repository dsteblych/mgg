﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Mgg.Common.DL.Entities;

using Microsoft.EntityFrameworkCore;

namespace Mgg.Common.DL
{
    public abstract class Repository<TTEntity, TId> : IRepository<TTEntity, TId> where TTEntity : Entity<TId> where TId : IEquatable<TId>
    {
        protected readonly IUnitOfWork UnitOfWork;

        protected Repository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public IQueryable<TTEntity> All => DbSet;

        public IEnumerable<TTEntity> AllAsEnumerable => DbSet.AsEnumerable();

        public DbSet<TTEntity> DbSet { get; }

        public TId Add(TTEntity newEntity) => DbSet.Add(newEntity).Entity.Id;

        public Maybe<TTEntity> FindById(TId id)
        {
            throw new NotImplementedException();
        }

        /// public Maybe<TTEntity> FindById(TId id) => DbSet.Where(x => x.Id == id);

        public TTEntity Load(TId id) => DbSet.Find(id);

        public void Remove(TTEntity entity) => DbSet.Remove(entity);

        public IQueryable<TTEntity> Where(Expression<Func<TTEntity, bool>> predicate) => DbSet.Where(predicate);
    }
}