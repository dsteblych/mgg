﻿namespace Mgg.Common.DL.Entities
{
    using JetBrains.Annotations;

    public class Item : Entity<int>
    {
        public int Order { get; set; }

        [NotNull]
        public string Text { get; set; }
    }
}