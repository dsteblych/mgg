﻿using JetBrains.Annotations;

namespace Mgg.Common.DL.Entities
{
    public interface IEntity<TId>
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The entity identifier.
        /// </value>
        [NotNull]
        TId Id { get; set; }
    }
}