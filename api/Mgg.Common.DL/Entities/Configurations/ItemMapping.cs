﻿using JetBrains.Annotations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mgg.Common.DL.Entities.Configurations
{
    internal class ItemMapping : IEntityTypeConfiguration<Item>
    {
        /// <summary>
        /// Configures the entity of type <typeparam name="Item"/>
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure([NotNull]EntityTypeBuilder<Item> builder)
        {
            builder.Property(i => i.Text)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}