﻿using JetBrains.Annotations;

using Mgg.Common.DL.Entities;

namespace Mgg.Common.DL
{
    public class ItemRepository : Repository<Item, int>, IItemRepository
    {
        public ItemRepository([NotNull]IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
