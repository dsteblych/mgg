using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <inheritdoc />
    /// <summary>
    /// Provide a logic for describing successful or failure operation.
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <seealso cref="T:Mgg.Common.Result" />
    public class Result<TSource> : Result
    {
        /// <summary>
        /// The value.
        /// </summary>
        [CanBeNull]
        private readonly TSource value;

        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Mgg.Common.Result`1" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="isSuccess">if set to <c>true</c> [is success].</param>
        /// <param name="errorMessage">The error message.</param>
        protected internal Result([CanBeNull] TSource value, bool isSuccess, [CanBeNull]string errorMessage) : base(isSuccess, errorMessage)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [CanBeNull]
        public TSource Value
        {
            get
            {
                if (IsFailure)
                {
                    throw Exceptions.CreateInvalidOperation(Strings.ResultIsNotSuccess);
                }

                return value;
            }
        }
    }
}