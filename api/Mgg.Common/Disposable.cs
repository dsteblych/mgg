﻿using System;

using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <summary>
    /// Provides a set of static methods for transform object that implementation <see cref="IDisposable" /> interface.
    /// </summary>
    public static class Disposable
    {
        /// <summary>
        /// The functional wrapper for <see cref="IDisposable" /> object.
        /// </summary>
        /// <typeparam name="TDisposable">The type of the disposable.</typeparam>
        /// <typeparam name="TResult">The type of the value returned by <paramref name="selector" />.</typeparam>
        /// <param name="factory">The factory that create <see cref="IDisposable" /> instance.</param>
        /// <param name="selector">A transform function to apply to <see cref="IDisposable" /> instance.</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="factory" /> or <paramref name="selector" /> is null.</exception>
        /// <returns>The <see cref="TResult"/> returned by <paramref name="selector" />.</returns>
        [NotNull]
        public static TResult Using<TDisposable, TResult>([NotNull]Func<TDisposable> factory, [NotNull]Func<TDisposable, TResult> selector) where TDisposable : IDisposable
        {
            if (factory == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(factory));
            }

            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            using (var disposable = factory())
            {
                return selector(disposable);
            }
        }
    }
}