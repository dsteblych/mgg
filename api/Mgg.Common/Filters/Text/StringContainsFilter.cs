﻿namespace Mgg.Common.Filters.Text
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    public class StringContainsFilter : BaseFilter
    {
        public override Expression Apply(Expression expression)
        {
            MethodInfo toUpperMethod = typeof(string).GetMethod("ToUpper", new Type[0]);
            MethodInfo containsMethod = typeof(string).GetMethod("Contains");
            Expression value = Expression.Constant(this.Value.ToUpper());

            Expression notNull = Expression.NotEqual(expression, Expression.Constant(null));
            Expression toUpper = Expression.Call(expression, toUpperMethod);

            Expression contains = Expression.Call(toUpper, containsMethod, value);

            return Expression.AndAlso(notNull, contains);
        }
    }
}
