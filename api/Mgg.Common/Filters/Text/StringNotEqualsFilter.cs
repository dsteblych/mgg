﻿namespace Mgg.Common.Filters.Text
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    public class StringNotEqualsFilter : BaseFilter
    {
        public override Expression Apply(Expression expression)
        {
            if (string.IsNullOrEmpty(this.Value))
            {
                Expression isNotEmpty = Expression.NotEqual(expression, Expression.Constant(""));
                Expression notNull = Expression.NotEqual(expression, Expression.Constant(null));

                return Expression.AndAlso(notNull, isNotEmpty);
            }

            MethodInfo toUpperMethod = typeof(string).GetMethod("ToUpper", new Type[0]);
            Expression value = Expression.Constant(this.Value.ToUpper());

            Expression equalsNull = Expression.Equal(expression, Expression.Constant(null));
            Expression toUpper = Expression.Call(expression, toUpperMethod);
            Expression notEquals = Expression.NotEqual(toUpper, value);

            return Expression.OrElse(equalsNull, notEquals);
        }
    }
}
