﻿namespace Mgg.Common.Filters.Text
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    public class StringEndsWithFilter : BaseFilter
    {
        public override Expression Apply(Expression expression)
        {
            MethodInfo endsWithMethod = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });
            MethodInfo toUpperMethod = typeof(string).GetMethod("ToUpper", new Type[0]);
            Expression value = Expression.Constant(this.Value.ToUpper());

            Expression notNull = Expression.NotEqual(expression, Expression.Constant(null));
            Expression toUpper = Expression.Call(expression, toUpperMethod);

            Expression endsWith = Expression.Call(toUpper, endsWithMethod, value);

            return Expression.AndAlso(notNull, endsWith);
        }
    }
}
