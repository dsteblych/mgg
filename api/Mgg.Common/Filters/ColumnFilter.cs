﻿
namespace Mgg.Common.Filters
{
    using System;
    using System.Linq.Expressions;

    using Mgg.Common.Filters.Columns;

    public class ColumnFilter<T> : IColumnFilter<T>
    {
        public string Operator { get; set; }

        public IFilter First { get; set; }

        public IFilter Second { get; set; }

        public IColumn<T> Column { get; set; }

        public Expression<Func<T, bool>> Process()
        {
            var expression = this.CreateFilterExpression();

            return expression == null ? null : this.ToLambda(expression);
        }

        private Expression CreateFilterExpression()
        {
            var rightExpression = this.Second?.Apply(this.Column.Expression.Body);
            var leftExpression = this.First?.Apply(this.Column.Expression.Body);

            if (leftExpression != null && rightExpression != null)
            {
                switch (this.Operator)
                {
                    case "And":
                        return Expression.AndAlso(leftExpression, rightExpression);
                    case "Or":
                        return Expression.OrElse(leftExpression, rightExpression);
                }
            }

            return leftExpression ?? rightExpression;
        }

        private Expression<Func<T, bool>> ToLambda(Expression expression)
        {
            return Expression.Lambda<Func<T, bool>>(expression, this.Column.Expression.Parameters[0]);
        }
    }
}
