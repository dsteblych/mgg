﻿
namespace Mgg.Common.Filters.Columns
{
    using System.Collections.Generic;

    public class Columns<T> : List<IColumn<T>>, IColumns<T>
    {
        public IGridParameters IGridParameters { get; set; }

        public Columns(IGridParameters GgidParametrs)
        {
            this.IGridParameters = GgidParametrs;
        }

        public void AddColumn(IColumn<T> column)
        {
            base.Add(column);
        }
    }
}
