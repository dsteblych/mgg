﻿
namespace Mgg.Common.Filters.Columns
{
    using System.Collections.Generic;

    public class ColumnsCollection<T> : List<IColumn<T>>, IColumns<T>
    {
        public ColumnsCollection(IGridParameters gridParameters)
        {
            this.IGridParameters = gridParameters;
        }

        public IGridParameters IGridParameters { get; set; }

        public void AddColumn(IColumn<T> column)
        {
            this.Add(column);
        }
    }
}
