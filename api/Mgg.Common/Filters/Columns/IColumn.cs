﻿
namespace Mgg.Common.Filters.Columns
{
    using System.Collections.Specialized;
    using System.Linq.Expressions;

    public interface IColumn : IFilterableColumn
    {
        string Name { get; set; }
    }

    public interface IColumn<T> : IFilterableColumn<T>
    {
        IGridParameters GridParameters { get; }

        string Name { get; set; }

        LambdaExpression Expression { get; }
    }

    public interface IGridParameters
    {
        string Name { get; set; }

        NameValueCollection Query { get; set; }
    }
}
