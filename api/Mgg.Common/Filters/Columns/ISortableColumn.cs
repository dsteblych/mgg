﻿
namespace Mgg.Common.Filters.Columns
{
    public interface ISortableColumn
    {
        bool? IsSortable { get; set; }

        GridSortOrder? SortOrder { get; set; }

        GridSortOrder? FirstSortOrder { get; set; }

        GridSortOrder? InitialSortOrder { get; set; }
    }
}
