﻿
namespace Mgg.Common.Filters.Columns
{
    using System.Collections.Generic;

    public interface IColumns<T> : IEnumerable<IColumn<T>>
    {
        IGridParameters IGridParameters { get; set; }

        void AddColumn(IColumn<T> column);
    }
}
