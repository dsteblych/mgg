﻿
namespace Mgg.Common.Filters.Columns
{
    public interface IFilterableColumn
    {
        string FilterName { get; set; }

        IColumnFilter Filter { get; }

        bool? IsFilterable { get; set; }

        bool? IsMultiFilterable { get; set; }
    }

    public interface IFilterableColumn<T> : IFilterableColumn
    {
        new IColumnFilter<T> Filter { get; set; }
    }
}
