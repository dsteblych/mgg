
namespace Mgg.Common.Filters.Columns
{
    using System;
    using System.Linq.Expressions;

    public class Column<T> : IColumn<T>
    {
        public IGridParameters GridParameters { get; set; }

        public string Name { get; set; }

        public LambdaExpression Expression { get; }

        public Type ColumnType { get; set; }

        private string GetFilterName()
        {
            var type = Nullable.GetUnderlyingType(this.ColumnType) ?? this.ColumnType;
            if (type.IsEnum)
            {
                return null;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return nameof(Number);
                case TypeCode.String:
                    return "Text";
                case TypeCode.DateTime:
                    return nameof(Date);
                case TypeCode.Boolean:
                    return nameof(Boolean);
                default:
                    return null;
            }
        }

        public string FilterName { get; set; }

        IColumnFilter IFilterableColumn.Filter { get; }

        public IColumnFilter<T> Filter { get; set; }

        public bool? IsFilterable { get; set; }

        public bool? IsMultiFilterable { get; set; }
    }
}
