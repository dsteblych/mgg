//using System;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Web;

//namespace Homsters.Common.GridFiltering.Columns
//{
//    using Boolean = System.Boolean;

//    public abstract class BaseColumn<T, TValue> : IColumn<T>
//    {
//        public string Name { get; set; }

//        public string Format { get; set; }

//        public string CssClasses { get; set; }

//        public bool IsEncoded { get; set; }

//        public IHtmlString Title { get; set; }

//        public bool? IsSortable { get; set; }

//        public GridSortOrder? FirstSortOrder { get; set; }

//        public GridSortOrder? InitialSortOrder { get; set; }

//        public virtual GridSortOrder? SortOrder { get; set; }

//        public string FilterName { get; set; }

//        public bool? IsFilterable { get; set; }

//        public bool? IsMultiFilterable { get; set; }

//        IColumnFilter IFilterableColumn.Filter => Filter;

//        public virtual IColumnFilter<T> Filter { get; set; }

//        public IGridParameters GridParameters { get; set; }

//        public Func<T, object> RenderValue { get; set; }

//        // public GridProcessorType ProcessorType { get; set; }
//        public Func<T, TValue> ExpressionValue { get; set; }

//        LambdaExpression IColumn<T>.Expression => Expression;

//        public Expression<Func<T, TValue>> Expression { get; set; }

//        public virtual IColumn<T> RenderedAs(Func<T, object> value)
//        {
//            RenderValue = value;

//            return this;
//        }

//        public virtual IColumn<T> MultiFilterable(bool isMultiple)
//        {
//            IsMultiFilterable = isMultiple;

//            return this;
//        }

//        public virtual IColumn<T> Filterable(bool isFilterable)
//        {
//            IsFilterable = isFilterable;

//            return this;
//        }

//        public virtual IColumn<T> FilteredAs(string filterName)
//        {
//            FilterName = filterName;

//            return this;
//        }

//        public virtual IColumn<T> InitialSort(GridSortOrder order)
//        {
//            InitialSortOrder = order;

//            return this;
//        }

//        public virtual IColumn<T> FirstSort(GridSortOrder order)
//        {
//            FirstSortOrder = order;

//            return this;
//        }

//        public virtual IColumn<T> Sortable(bool isSortable)
//        {
//            IsSortable = isSortable;

//            return this;
//        }

//        public virtual IColumn<T> Encoded(bool isEncoded)
//        {
//            IsEncoded = isEncoded;

//            return this;
//        }

//        public virtual IColumn<T> Formatted(string format)
//        {
//            Format = format;

//            return this;
//        }

//        public virtual IColumn<T> Css(string cssClasses)
//        {
//            CssClasses = cssClasses;

//            return this;
//        }

//        public virtual IColumn<T> Titled(object value)
//        {
//            Title = value as IHtmlString ?? new HtmlString(value?.ToString());

//            return this;
//        }

//        public virtual IColumn<T> Named(string name)
//        {
//            Name = name;

//            return this;
//        }

//        public abstract IQueryable<T> Process(IQueryable<T> items);

//        // public abstract IHtmlString ValueFor(IGridRow<Object> row);
//    }
//}
