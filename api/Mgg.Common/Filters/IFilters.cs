﻿
namespace Mgg.Common.Filters
{
    using System;

    using Mgg.Common.Filters.Columns;

    public interface IFilters
    {
        IColumnFilter<T> GetFilter<T>(IColumn<T> column);

        void Register(Type forType, string filterType, Type filter);

        void Unregister(Type forType, string filterType);
    }
}
