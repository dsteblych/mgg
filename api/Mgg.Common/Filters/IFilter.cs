﻿
namespace Mgg.Common.Filters
{
    using System.Linq.Expressions;

    public interface IFilter
    {
        string Type { get; set; }

        string Value { get; set; }

        Expression Apply(Expression expression);
    }
}