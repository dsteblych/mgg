﻿
namespace Mgg.Common.Filters
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Linq.Expressions;

    using Mgg.Common.Filters.Columns;

    public class FilteringExpression<T>
    {
        private readonly IGridParameters _gridParameters = new GridParameters { Name = nameof(GridParameters) };

        private readonly Filters filters = new Filters();

        public FilteringExpression(NameValueCollection inputCollection)
        {
            if (inputCollection == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(inputCollection));
            }

            this._gridParameters.Query = inputCollection;
            this.Columns = new ColumnsCollection<IColumn<T>>(this._gridParameters);

            var columns = typeof(T).GetProperties()
                .Select(
                    prop => new Column<IColumn<T>>
                    {
                        Name = prop.Name,
                        GridParameters = this._gridParameters
                    })
                .ToList();

            foreach (var column in columns)
            {
                this.Columns.AddColumn(column);
            }
        }

        public IColumns<IColumn<T>> Columns { get; set; }

        public Expression<Func<T, bool>> Get()
        {
            Expression<Func<T, bool>> resultFilter = e => true;

            return this.Columns.Select(col => this.filters.GetFilter(col).Process()).Where(f => f != null).Aggregate(resultFilter, (current, filter) => (Expression<Func<T, bool>>)Expression.AndAlso(current, filter).Reduce());
        }
    }
}
