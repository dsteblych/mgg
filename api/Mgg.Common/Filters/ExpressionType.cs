﻿
namespace Mgg.Common.Filters
{
    public static class ExpressionType
    {
        public new const string Equals = "Equals";
        public const string NotEquals = "NotEquals";
        public const string LessThan = "LessThan";
        public const string GreaterThan = "GreaterThan";
        public const string LessThanOrEqual = "LessThanOrEqual";
        public const string GreaterThanOrEqual = "GreaterThanOrEqual";
    }
}
