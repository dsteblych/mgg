﻿using System.Linq.Expressions;

namespace Mgg.Common.Filters
{
    public abstract class BaseFilter : IFilter
    {
        public string Type { get; set; }

        public string Value { get; set; }

        public abstract Expression Apply(Expression expression);
    }
}
