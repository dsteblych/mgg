﻿
namespace Mgg.Common.Filters
{
    using System;
    using System.Linq.Expressions;

    using Mgg.Common.Filters.Columns;

    public interface IColumnFilter
    {
        string Operator { get; set; }

        IFilter First { get; set; }

        IFilter Second { get; set; }
    }

    public interface IColumnFilter<T> : IColumnFilter
    {
        IColumn<T> Column { get; set; }

        Expression<Func<T, bool>> Process();
    }
}
