﻿
namespace Mgg.Common.Filters.Number
{
    public class DoubleFilter : NumberFilter
    {
        /// <summary>
        /// Gets the double value, if value can be parsed.
        /// </summary>
        /// <value>
        /// The double value.
        /// </value>
        public override object NumericValue
        {
            get
            {
                if (double.TryParse(this.Value, out var number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}