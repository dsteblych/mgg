﻿
namespace Mgg.Common.Filters.Number
{
    public class SingleFilter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                float number;
                if (float.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
