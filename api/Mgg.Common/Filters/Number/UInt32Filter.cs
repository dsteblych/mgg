﻿
namespace Mgg.Common.Filters.Number
{
    public class UInt32Filter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                uint number;
                if (uint.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
