﻿
namespace Mgg.Common.Filters.Number
{
    public class Int64Filter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                long number;
                if (long.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
