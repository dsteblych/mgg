﻿
namespace Mgg.Common.Filters.Number
{
    using System.Linq.Expressions;

    using JetBrains.Annotations;

    using ExpressionType = Mgg.Common.Filters.ExpressionType;

    public abstract class NumberFilter : BaseFilter
    {
        [CanBeNull]
        public abstract object NumericValue { get; }

        public override Expression Apply(Expression expression)
        {
            if (expression == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(expression));
            }

            var value = this.NumericValue;

            if (value == null)
            {
                return null;
            }

            switch (this.Type)
            {
                case ExpressionType.Equals:
                    return Expression.Equal(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.NotEquals:
                    return Expression.NotEqual(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.LessThan:
                    return Expression.LessThan(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.GreaterThan:
                    return Expression.GreaterThan(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.LessThanOrEqual:
                    return Expression.LessThanOrEqual(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(expression, Expression.Constant(value, expression.Type));
                default:
                    return null;
            }
        }
    }
}
