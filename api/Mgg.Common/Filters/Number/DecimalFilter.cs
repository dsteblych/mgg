﻿
namespace Mgg.Common.Filters.Number
{
    public class DecimalFilter : NumberFilter
    {
        /// <summary>
        /// Gets the decimal value, if value can be parsed.
        /// </summary>
        /// <value>
        /// The decimal value.
        /// </value>
        public override object NumericValue
        {
            get
            {
                if (decimal.TryParse(this.Value, out var number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}