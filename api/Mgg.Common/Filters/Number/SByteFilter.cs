﻿
namespace Mgg.Common.Filters.Number
{
    public class SByteFilter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                sbyte number;
                if (sbyte.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
