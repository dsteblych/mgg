﻿
namespace Mgg.Common.Filters.Number
{
    /// <summary>
    /// Gets the int value, if value can be parsed.
    /// </summary>
    /// <value>
    /// The int value.
    /// </value>
    public class Int32Filter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                if (int.TryParse(Value, out var number))
                {
                    return number;
                }

                return null;
            }
        }
    }
} 