﻿
namespace Mgg.Common.Filters.Number
{
    public class UInt16Filter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                ushort number;
                if (ushort.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
