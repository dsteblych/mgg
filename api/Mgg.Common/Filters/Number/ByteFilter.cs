﻿namespace Mgg.Common.Filters.Number
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Mgg.Common.Filters.Number.NumberFilter" />
    public class ByteFilter : NumberFilter
    {
        /// <summary>
        /// Gets the byte value, if value can be parsed.
        /// </summary>
        /// <value>
        /// The byte value.
        /// </value>
        public override object NumericValue
        {
            get
            {
                if (byte.TryParse(Value, out var number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}