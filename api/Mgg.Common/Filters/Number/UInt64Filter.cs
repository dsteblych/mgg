﻿
namespace Mgg.Common.Filters.Number
{
    public class UInt64Filter : NumberFilter
    {
        public override object NumericValue
        {
            get
            {
                ulong number;
                if (ulong.TryParse(this.Value, out number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}
