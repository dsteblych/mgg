﻿
namespace Mgg.Common.Filters.Number
{
    public class Int16Filter : NumberFilter
    {
        /// <summary>
        /// Gets the short value, if value can be parsed.
        /// </summary>
        /// <value>
        /// The short value.
        /// </value>
        public override object NumericValue
        {
            get
            {
                if (short.TryParse(this.Value, out var number))
                {
                    return number;
                }

                return null;
            }
        }
    }
}