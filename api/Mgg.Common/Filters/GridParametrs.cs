﻿
namespace Mgg.Common.Filters
{
    using System.Collections.Specialized;

    using Mgg.Common.Filters.Columns;

    internal class GridParameters : IGridParameters
    {
        public string Name { get; set; }

        public NameValueCollection Query { get; set; }
    }
}
