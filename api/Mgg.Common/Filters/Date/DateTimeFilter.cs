﻿
namespace Mgg.Common.Filters.Date
{
    using System;
    using System.Linq.Expressions;

    using ExpressionType = Mgg.Common.Filters.ExpressionType;

    public class DateTimeFilter : BaseFilter
    {
        public override Expression Apply(Expression expression)
        {
            if (expression == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(expression));
            }

            var value = this.GetDateValue();
            if (value == null)
            {
                return null;
            }

            switch (this.Type)
            {
                case ExpressionType.Equals:
                    return Expression.Equal(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.NotEquals:
                    return Expression.NotEqual(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.LessThan:
                    return Expression.LessThan(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.GreaterThan:
                    return Expression.GreaterThan(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.LessThanOrEqual:
                    return Expression.LessThanOrEqual(expression, Expression.Constant(value, expression.Type));
                case ExpressionType.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(expression, Expression.Constant(value, expression.Type));
                default:
                    return null;
            }
        }

        private object GetDateValue()
        {
            DateTime date;
            if (DateTime.TryParse(this.Value, out date))
            {
                return date;
            }

            return null;
        }
    }
}
