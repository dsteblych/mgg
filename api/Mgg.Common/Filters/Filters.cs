﻿
namespace Mgg.Common.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Mgg.Common.Filters.Boolean;
    using Mgg.Common.Filters.Columns;
    using Mgg.Common.Filters.Date;
    using Mgg.Common.Filters.Number;
    using Mgg.Common.Filters.Text;

    public class Filters : IFilters
    {
        public Filters()
        {
            this.Table = new Dictionary<Type, IDictionary<string, Type>>();

            this.Register(typeof(sbyte), ExpressionType.Equals, typeof(SByteFilter));
            this.Register(typeof(sbyte), ExpressionType.NotEquals, typeof(SByteFilter));
            this.Register(typeof(sbyte), ExpressionType.LessThan, typeof(SByteFilter));
            this.Register(typeof(sbyte), ExpressionType.GreaterThan, typeof(SByteFilter));
            this.Register(typeof(sbyte), ExpressionType.LessThanOrEqual, typeof(SByteFilter));
            this.Register(typeof(sbyte), ExpressionType.GreaterThanOrEqual, typeof(SByteFilter));

            this.Register(typeof(byte), ExpressionType.Equals, typeof(ByteFilter));
            this.Register(typeof(byte), ExpressionType.NotEquals, typeof(ByteFilter));
            this.Register(typeof(byte), ExpressionType.LessThan, typeof(ByteFilter));
            this.Register(typeof(byte), ExpressionType.GreaterThan, typeof(ByteFilter));
            this.Register(typeof(byte), ExpressionType.LessThanOrEqual, typeof(ByteFilter));
            this.Register(typeof(byte), ExpressionType.GreaterThanOrEqual, typeof(ByteFilter));

            this.Register(typeof(short), ExpressionType.Equals, typeof(Int16Filter));
            this.Register(typeof(short), ExpressionType.NotEquals, typeof(Int16Filter));
            this.Register(typeof(short), ExpressionType.LessThan, typeof(Int16Filter));
            this.Register(typeof(short), ExpressionType.GreaterThan, typeof(Int16Filter));
            this.Register(typeof(short), ExpressionType.LessThanOrEqual, typeof(Int16Filter));
            this.Register(typeof(short), ExpressionType.GreaterThanOrEqual, typeof(Int16Filter));

            this.Register(typeof(ushort), ExpressionType.Equals, typeof(UInt16Filter));
            this.Register(typeof(ushort), ExpressionType.NotEquals, typeof(UInt16Filter));
            this.Register(typeof(ushort), ExpressionType.LessThan, typeof(UInt16Filter));
            this.Register(typeof(ushort), ExpressionType.GreaterThan, typeof(UInt16Filter));
            this.Register(typeof(ushort), ExpressionType.LessThanOrEqual, typeof(UInt16Filter));
            this.Register(typeof(ushort), ExpressionType.GreaterThanOrEqual, typeof(UInt16Filter));

            this.Register(typeof(int), "Equals", typeof(Int32Filter));
            this.Register(typeof(int), "NotEquals", typeof(Int32Filter));
            this.Register(typeof(int), "LessThan", typeof(Int32Filter));
            this.Register(typeof(int), "GreaterThan", typeof(Int32Filter));
            this.Register(typeof(int), "LessThanOrEqual", typeof(Int32Filter));
            this.Register(typeof(int), "GreaterThanOrEqual", typeof(Int32Filter));

            this.Register(typeof(uint), "Equals", typeof(UInt32Filter));
            this.Register(typeof(uint), "NotEquals", typeof(UInt32Filter));
            this.Register(typeof(uint), "LessThan", typeof(UInt32Filter));
            this.Register(typeof(uint), "GreaterThan", typeof(UInt32Filter));
            this.Register(typeof(uint), "LessThanOrEqual", typeof(UInt32Filter));
            this.Register(typeof(uint), "GreaterThanOrEqual", typeof(UInt32Filter));

            this.Register(typeof(long), "Equals", typeof(Int64Filter));
            this.Register(typeof(long), "NotEquals", typeof(Int64Filter));
            this.Register(typeof(long), "LessThan", typeof(Int64Filter));
            this.Register(typeof(long), "GreaterThan", typeof(Int64Filter));
            this.Register(typeof(long), "LessThanOrEqual", typeof(Int64Filter));
            this.Register(typeof(long), "GreaterThanOrEqual", typeof(Int64Filter));

            this.Register(typeof(ulong), "Equals", typeof(UInt64Filter));
            this.Register(typeof(ulong), "NotEquals", typeof(UInt64Filter));
            this.Register(typeof(ulong), "LessThan", typeof(UInt64Filter));
            this.Register(typeof(ulong), "GreaterThan", typeof(UInt64Filter));
            this.Register(typeof(ulong), "LessThanOrEqual", typeof(UInt64Filter));
            this.Register(typeof(ulong), "GreaterThanOrEqual", typeof(UInt64Filter));

            this.Register(typeof(float), "Equals", typeof(SingleFilter));
            this.Register(typeof(float), "NotEquals", typeof(SingleFilter));
            this.Register(typeof(float), "LessThan", typeof(SingleFilter));
            this.Register(typeof(float), "GreaterThan", typeof(SingleFilter));
            this.Register(typeof(float), "LessThanOrEqual", typeof(SingleFilter));
            this.Register(typeof(float), "GreaterThanOrEqual", typeof(SingleFilter));

            this.Register(typeof(double), "Equals", typeof(DoubleFilter));
            this.Register(typeof(double), "NotEquals", typeof(DoubleFilter));
            this.Register(typeof(double), "LessThan", typeof(DoubleFilter));
            this.Register(typeof(double), "GreaterThan", typeof(DoubleFilter));
            this.Register(typeof(double), "LessThanOrEqual", typeof(DoubleFilter));
            this.Register(typeof(double), "GreaterThanOrEqual", typeof(DoubleFilter));

            this.Register(typeof(decimal), "Equals", typeof(DecimalFilter));
            this.Register(typeof(decimal), "NotEquals", typeof(DecimalFilter));
            this.Register(typeof(decimal), "LessThan", typeof(DecimalFilter));
            this.Register(typeof(decimal), "GreaterThan", typeof(DecimalFilter));
            this.Register(typeof(decimal), "LessThanOrEqual", typeof(DecimalFilter));
            this.Register(typeof(decimal), "GreaterThanOrEqual", typeof(DecimalFilter));

            this.Register(typeof(DateTime), "Equals", typeof(DateTimeFilter));
            this.Register(typeof(DateTime), "NotEquals", typeof(DateTimeFilter));
            this.Register(typeof(DateTime), "LessThan", typeof(DateTimeFilter));
            this.Register(typeof(DateTime), "GreaterThan", typeof(DateTimeFilter));
            this.Register(typeof(DateTime), "LessThanOrEqual", typeof(DateTimeFilter));
            this.Register(typeof(DateTime), "GreaterThanOrEqual", typeof(DateTimeFilter));

            this.Register(typeof(bool), "Equals", typeof(BooleanFilter));

            this.Register(typeof(string), "Equals", typeof(StringEqualsFilter));
            this.Register(typeof(string), "NotEquals", typeof(StringNotEqualsFilter));
            this.Register(typeof(string), "Contains", typeof(StringContainsFilter));
            this.Register(typeof(string), "EndsWith", typeof(StringEndsWithFilter));
            this.Register(typeof(string), "StartsWith", typeof(StringStartsWithFilter));
        }

        public IDictionary<Type, IDictionary<string, Type>> Table { get; }

        public IColumnFilter<T> GetFilter<T>(IColumn<T> column)
        {
            if (column == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(column));
            }

            var keys = column
                .GridParameters
                .Query
                .AllKeys
                .Where(key =>
                    (key ?? "").StartsWith(column.GridParameters.Name + "-" + column.Name + "-") &&
                    key != column.GridParameters.Name + "-" + column.Name + "-Op")
                .ToArray();

            var filter = new ColumnFilter<T>
            {
                Second = this.GetSecondFilter(column, keys),
                First = this.GetFirstFilter(column, keys),
                Operator = GetOperator(column),
                Column = column
            };

            return filter;
        }

        public void Register(Type forType, string filterType, Type filter)
        {
            IDictionary<string, Type> filters = new Dictionary<string, Type>();
            var underlyingType = Nullable.GetUnderlyingType(forType) ?? forType;

            if (this.Table.ContainsKey(underlyingType))
            {
                filters = this.Table[underlyingType];
            }
            else
            {
                this.Table[underlyingType] = filters;
            }

            filters[filterType] = filter;
        }

        public void Unregister(Type forType, string filterType)
        {
            if (this.Table.ContainsKey(forType))
            {
                this.Table[forType].Remove(filterType);
            }
        }

        private static string GetOperator<T>(IColumn<T> column)
        {
            var values = column.GridParameters.Query.GetValues(column.GridParameters.Name + "-" + column.Name + "-Op");
            if (column.IsMultiFilterable != true || values == null)
            {
                return null;
            }

            return values[0];
        }

        private IFilter GetFilter<T>(IColumn<T> column, string type, string value)
        {
            var valueType = Nullable.GetUnderlyingType(column.Expression.ReturnType) ?? column.Expression.ReturnType;
            if (!this.Table.ContainsKey(valueType))
            {
                return null;
            }

            var typedFilters = this.Table[valueType];
            if (!typedFilters.ContainsKey(type))
            {
                return null;
            }

            var filter = (IFilter)Activator.CreateInstance(typedFilters[type]);
            filter.Value = value;
            filter.Type = type;

            return filter;
        }

        private IFilter GetSecondFilter<T>(IColumn<T> column, string[] keys)
        {
            if (column.IsMultiFilterable != true || keys.Length == 0)
            {
                return null;
            }

            if (keys.Length == 1)
            {
                var values = column.GridParameters.Query.GetValues(keys[0]);
                if (values != null && values.Length < 2)
                {
                    return null;
                }

                if (values == null)
                {
                    return null;
                }

                var keyType = keys[0].Substring((column.GridParameters.Name + "-" + column.Name + "-").Length);

                return this.GetFilter(column, keyType, values[1]);
            }

            var type = keys[1].Substring((column.GridParameters.Name + "-" + column.Name + "-").Length);
            var strings = column.GridParameters.Query.GetValues(keys[1]);

            if (strings != null)
            {
                var value = strings[0];

                return this.GetFilter(column, type, value);
            }

            return null;
        }

        private IFilter GetFirstFilter<T>(IColumn<T> column, string[] keys)
        {
            if (keys.Length == 0)
            {
                return null;
            }

            var type = keys[0].Substring((column.GridParameters.Name + "-" + column.Name + "-").Length);
            var strings = column.GridParameters.Query.GetValues(keys[0]);
            if (strings != null)
            {
                var value = strings[0];

                return this.GetFilter(column, type, value);
            }

            return null;
        }
    }
}
