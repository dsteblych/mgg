﻿using System;
using System.Linq.Expressions;
using JetBrains.Annotations;

namespace Mgg.Common.Filters.Boolean
{
    public class BooleanFilter : BaseFilter
    {
        [CanBeNull]
        public override Expression Apply([NotNull]Expression expression)
        {
            if (expression == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(expression));
            }

            var value = GetBooleanValue();

            return value == null ? null : Expression.Equal(expression, Expression.Constant(value, expression.Type));
        }

        [CanBeNull]
        private object GetBooleanValue()
        {
            if (string.Equals(Value, "true", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            if (string.Equals(Value, "false", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return null;
        }
    }
}