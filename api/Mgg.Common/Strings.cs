﻿using System.Diagnostics.CodeAnalysis;

namespace Mgg.Common
{
    /// <summary>
    /// Container that contains common strings constants
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1600", Justification = "Fields is self-comments")]
    public static class Strings
    {
        public const string ResultIsNotSuccess = "Result is not success";
        public const string NoValue = "No value";
        public const string ValueCannotBeNull = "Value cannot be null";
    }
}