﻿using JetBrains.Annotations;

namespace Mgg.Common
{
    // ToDo Add xml comments
    public abstract class ValueObject<TSource> where TSource : ValueObject<TSource>
    {
        public static bool operator ==([CanBeNull]ValueObject<TSource> first, [CanBeNull]ValueObject<TSource> second)
        {
            if (ReferenceEquals(first, null) && ReferenceEquals(second, null))
            {
                return true;
            }

            if (ReferenceEquals(first, null) || ReferenceEquals(second, null))
            {
                return false;
            }

            return first.Equals(second);
        }

        public static bool operator !=([CanBeNull]ValueObject<TSource> first, [CanBeNull]ValueObject<TSource> second) => !(first == second);

        public override int GetHashCode() => GetHashCodeCore();

        public override bool Equals(object obj)
        {
            var valueObject = obj as TSource;

            return !ReferenceEquals(valueObject, null) && EqualsCore(valueObject);
        }

        protected abstract int GetHashCodeCore();

        protected abstract bool EqualsCore([CanBeNull]TSource other);
    }
}