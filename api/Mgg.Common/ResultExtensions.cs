﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Mgg.Common
{
    // ToDo Add xml comments
    public static class ResultExtensions
    {
        [NotNull]
        public static Result<TSource> ToResult<TSource>(this Maybe<TSource> maybe, [NotNull]string errorMessage) where TSource : class
        {
            return maybe.HasNoValue ? Result.Fail<TSource>(errorMessage) : Result.Ok(maybe.Value);
        }

        [NotNull]
        public static Result<TResult> OnSuccess<TSource, TResult>([NotNull]this Result<TSource> result, [NotNull] Func<TSource, TResult> selector)
        {
            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            return result.IsFailure ? Result.Fail<TResult>(result.ErrorMessage) : Result.Ok(selector(result.Value));
        }

        [NotNull]
        public static Result<TSource> OnSuccess<TSource>([NotNull]this Result<TSource> result, [NotNull] Action<TSource> action)
        {
            if (action == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(action));
            }

            if (result.IsSuccess)
            {
                action(result.Value);
            }

            return result;
        }

        [NotNull]
        public static Result OnSuccess([NotNull]this Result result, [NotNull] Action action)
        {
            if (action == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(action));
            }

            if (result.IsSuccess)
            {
                action();
            }

            return result;
        }

        [NotNull]
        public static Result<TSource> Ensure<TSource>([NotNull]this Result<TSource> result, [NotNull] Func<TSource, bool> predicate, [NotNull]string errorMessage)
        {
            if (predicate == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(predicate));
            }

            if (result.IsFailure)
            {
                return result;
            }

            return !predicate(result.Value) ? Result.Fail<TSource>(errorMessage) : result;
        }

        [NotNull]
        public static TSource OnBoth<TSource>([NotNull]this Result result, [NotNull] Func<Result, TSource> selector)
        {
            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            return selector(result);
        }

        [NotNull]
        [SuppressMessage("ReSharper", "MethodNameNotMeaningful", Justification = "Correct domain methods name")]
        public static Result<TResult> Map<TSource, TResult>([NotNull]this Result<TSource> result, [NotNull] Func<TSource, TResult> selector)
        {
            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            return result.IsFailure ? Result.Fail<TResult>(result.ErrorMessage) : Result.Ok(selector(result.Value));
        }

        [NotNull]
        [SuppressMessage("ReSharper", "MethodNameNotMeaningful", Justification = "Correct domain methods name")]
        public static Result<TSource> When<TSource>([NotNull]this Result<TSource> result, [NotNull] Func<TSource, bool> predicate, [NotNull] Func<TSource, TSource> selector)
        {
            if (predicate == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(predicate));
            }

            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            return result.IsFailure
                ? Result.Fail<TSource>(result.ErrorMessage)
                : predicate(result.Value) ? Result.Ok(selector(result.Value)) : Result.Ok(result.Value);
        }

        [NotNull]
        [SuppressMessage("ReSharper", "MethodNameNotMeaningful", Justification = "Correct domain methods name")]
        public static Result<TSource> Tee<TSource>([NotNull]this Result<TSource> result, [NotNull]Action<Result<TSource>> action)
        {
            if (action == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(action));
            }

            action(result);

            return result;
        }
    }
}