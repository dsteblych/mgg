﻿using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <summary>
    /// Provide a logic for describing successful or failure operation.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="isSuccess">if set to <c>true</c> [operation is success].</param>
        /// <param name="errorMessage">The error message.</param>
        protected Result(bool isSuccess, [CanBeNull]string errorMessage)
        {
            if (isSuccess && !string.IsNullOrEmpty(errorMessage))
            {
                throw Exceptions.CreateInvalidOperation();
            }

            if (!isSuccess && string.IsNullOrEmpty(errorMessage))
            {
                throw Exceptions.CreateInvalidOperation();
            }

            IsSuccess = isSuccess;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets a value indicating whether operation is success.
        /// </summary>
        /// <value>
        /// <c>true</c> if operation is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess { get; }

        /// <summary>
        /// Gets the error message  if operation is failure.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        [CanBeNull]
        public string ErrorMessage { get; }

        /// <summary>
        /// Gets a value indicating whether operation is failure.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is failure; otherwise, <c>false</c>.
        /// </value>
        public bool IsFailure => !IsSuccess;

        /// <summary>
        /// Create <see cref="Result"/> instance that indicating failure operation.
        /// </summary>
        /// <param name="errorMessage">The  error message.</param>
        /// <returns>The <see cref="Result"/> instance that indicating failure operation.</returns>
        [NotNull]
        public static Result Fail([NotNull]string errorMessage) => new Result(false, errorMessage);

        /// <summary>
        /// Create <see cref="Result"/> instance that indicating failure operation.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="errorMessage">The  error message.</param>
        /// <returns>The <see cref="Result{TSource}"/> instance that indicating failure operation.</returns>
        [NotNull]
        public static Result<TSource> Fail<TSource>([NotNull]string errorMessage) => new Result<TSource>(default(TSource), false, errorMessage);

        /// <summary>
        /// Create <see cref="Result"/> instance that indicating successful operation.
        /// </summary>
        /// <returns>The <see cref="Result"/> instance that indicating successful operation.</returns>
        [SuppressMessage("ReSharper", "MethodNameNotMeaningful", Justification = "Correct domain methods name")]
        [NotNull]
        public static Result Ok() => new Result(true, string.Empty);

        /// <summary>
        /// Create <see cref="Result"/> instance that indicating successful operation.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="Result"/> instance that indicating successful operation.</returns>
        [SuppressMessage("ReSharper", "MethodNameNotMeaningful", Justification = "Correct domain methods name")]
        [NotNull]
        public static Result<TSource> Ok<TSource>([NotNull] TSource value) => new Result<TSource>(value, true, string.Empty);

        /// <summary>
        /// Combines the specified results.
        /// </summary>
        /// <param name="results">The results objects.</param>
        /// <returns>The combined <see cref="Result"/>.</returns>
        [NotNull]
        public static Result Combine([NotNull][ItemNotNull] params Result[] results)
        {
            foreach (var result in results)
            {
                if (result.IsFailure)
                {
                    return result;
                }
            }

            return Ok();
        }
    }
}