﻿using System;
using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <summary>
    /// Provide a container for objects that can be null.
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    public struct Maybe<TSource> : IEquatable<Maybe<TSource>> where TSource : class
    {
        /// <summary>
        /// The value of the <see cref="Maybe{TSource}"/> struct.
        /// </summary>
        [CanBeNull]
        private readonly TSource value;

        /// <summary>
        /// Initializes a new instance of the <see cref="Maybe{TSource}"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private Maybe([CanBeNull] TSource value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value of the <see cref="Maybe{TSource}"/> struct.
        /// </value>
        /// <exception cref="InvalidOperationException">
        /// HasNoValue is <c>true</c>.</exception>
        [NotNull]
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "Checked if value is equal null.")]
        public TSource Value
        {
            get
            {
                if (HasNoValue)
                {
                    throw Exceptions.CreateInvalidOperation();
                }

                return value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether value isn't null.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has value; otherwise, <c>false</c>.
        /// </value>
        public bool HasValue => value != null;

        /// <summary>
        /// Gets a value indicating whether value is null.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has no value; otherwise, <c>false</c>.
        /// </value>
        public bool HasNoValue => !HasValue;

        /// <summary>
        /// Performs an implicit conversion from <see cref="TSource"/> to <see cref="Maybe{TSource}"/>.
        /// </summary>
        /// <param name="value">The conversion value.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Maybe<TSource>([CanBeNull] TSource value) => new Maybe<TSource>(value);

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="maybe">The maybe.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Maybe<TSource> maybe, [CanBeNull] TSource value) => !maybe.HasNoValue && maybe.Value.Equals(value);

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="maybe">The maybe.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Maybe<TSource> maybe, [CanBeNull] TSource value) => !(maybe == value);

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Maybe<TSource> first, Maybe<TSource> second) => first.Equals(second);

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Maybe<TSource> first, Maybe<TSource> second) => !(first == second);

        /// <summary>
        /// Determines whether the specified <see cref="Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="Object" /> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Maybe<TSource>))
            {
                return false;
            }

            var other = (Maybe<TSource>)obj;

            return Equals(other);
        }

        /// <inheritdoc />
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other">other</paramref> parameter; otherwise, false.
        /// </returns>
        [SuppressMessage("ReSharper", "PossibleNullReferenceException", Justification = "Checked if value is equal null.")]
        public bool Equals(Maybe<TSource> other)
        {
            if (HasNoValue && other.HasNoValue)
            {
                return true;
            }

            if (HasNoValue || other.HasNoValue)
            {
                return false;
            }

            return value.Equals(other.value);
        }

        /// <summary>
        /// Unwraps the specified selector.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="selector">The selector.</param>
        /// <returns>The <see cref="TResult"/> returned by <paramref name="selector" />.</returns>
        [CanBeNull]
        public TResult Unwrap<TResult>([NotNull]Func<TSource, TResult> selector)
        {
            if (selector == null)
            {
                throw Exceptions.CreateArgumentNullException(nameof(selector));
            }

            return HasValue ? selector(Value) : default(TResult);
        }

        /// <summary>
        /// Unwraps this instance.
        /// </summary>
        /// <returns>The <see cref="TSource"/> value.</returns>
        [CanBeNull]
        public TSource Unwrap() => HasValue ? Value : default(TSource);

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        [SuppressMessage("ReSharper", "PossibleNullReferenceException", Justification = "Checked if value is equal null.")]
        public override int GetHashCode() => HasNoValue ? base.GetHashCode() : value.GetHashCode();

        /// <summary>
        /// Returns a <see cref="String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="String" /> that represents this instance.
        /// </returns>
        public override string ToString() => HasNoValue ? Strings.NoValue : Value.ToString();
    }
}