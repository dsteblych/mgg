﻿using System;
using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <summary>
    /// The exceptions factory class.
    /// </summary>
    public static class Exceptions
    {
        /// <summary>
        /// Create new instance of <see cref="ArgumentNullException"/>.
        /// </summary>
        /// <param name="argumentName">Argument name.</param>
        /// <returns>New instance of <see cref="ArgumentNullException"/>.</returns>
        [NotNull]
        public static ArgumentNullException CreateArgumentNullException([NotNull]string argumentName) => new ArgumentNullException($"Argument {argumentName} is required");

        /// <summary>
        /// Create new instance of <see cref="InvalidOperationException"/>.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <returns>New instance of <see cref="ArgumentNullException"/>.</returns>
        [NotNull]
        public static InvalidOperationException CreateInvalidOperation([NotNull]string message) => new InvalidOperationException(message);

        /// <summary>
        /// Create new instance of <see cref="InvalidOperationException"/>.
        /// </summary>
        /// <returns>New instance of <see cref="InvalidOperationException"/>.</returns>
        [NotNull]
        public static InvalidOperationException CreateInvalidOperation() => new InvalidOperationException();
    }
}