﻿using JetBrains.Annotations;

namespace Mgg.Common
{
    /// <summary>
    /// Provides a set of static methods for validation object that implementation <see cref="Result{TSource}" /> interface.
    /// </summary>
    public static class Validator
    {
        /// <summary>
        /// Determines whether [is not null] [the specified value].
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>The result of the validation.</returns>
        [NotNull]
        public static Result<TSource> IsNotNull<TSource>([CanBeNull]TSource value) where TSource : class => value == null ? Result.Fail<TSource>(Strings.ValueCannotBeNull) : Result.Ok(value);
    }
}